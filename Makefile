.DEFAULT_GOAL := all

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen
SHELL         := bash

ifeq ($(shell uname -s), Darwin)
    BOOST    := /usr/local/include/boost
    CXX      := clang++
    CXXFLAGS := --coverage -g -std=c++20 -I/usr/local/include/ -Wall -Wextra -Wpedantic
    GCOV     := llvm-cov gcov
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main
    LIB      := /usr/local/lib
    VALGRIND := valgrind
else ifeq ($(shell uname -p), unknown)
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -g -std=c++20 -Wall -Wextra -Wpedantic
    GCOV     := gcov
    GTEST    := /usr/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/lib
    VALGRIND := valgrind
else
    BOOST    := /usr/local/opt/boost-1.67/include/boost
    CXX      := g++-11
    CXXFLAGS := --coverage -g -std=c++20 -Wall -Wextra -Wpedantic
    GCOV     := gcov-11
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -L/usr/local/opt/boost-1.77/lib/ -lgtest -lgtest_main -pthread
    LIB      := /usr/local/lib
    VALGRIND := valgrind-3.17
endif

# run docker
docker:
	docker run --rm -it -v "$(PWD):/usr/gcc" -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Darwin.log.txt:
	git log > Darwin.log.txt

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Darwin code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Darwin code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Darwin.cpp
	git add Darwin.hpp
	-git add Darwin.log.txt
	-git add html
	git add Makefile
	git add README.md
	git add RunDarwin.cpp
	git add RunDarwin.ctd.txt
	git add TestDarwin.cpp
	git commit -m "another commit"
	git push
	git status

# compile run harness
RunDarwin: Darwin.hpp Darwin.cpp Species.cpp Species.hpp Creature.cpp Creature.hpp Instruction.hpp Direction.hpp RunDarwin.cpp
	-$(CPPCHECK) Darwin.cpp
	-$(CPPCHECK) Species.cpp
	-$(CPPCHECK) Creature.cpp
	-$(CPPCHECK) RunDarwin.cpp
	$(CXX) $(CXXFLAGS) Darwin.cpp Darwin.hpp Species.cpp Species.hpp Creature.cpp Creature.hpp Instruction.hpp Direction.hpp RunDarwin.cpp -o RunDarwin

# compile test harness
TestDarwin: Darwin.hpp Darwin.cpp Species.cpp Creature.cpp Species.hpp Creature.hpp TestDarwin.cpp
	-$(CPPCHECK) Darwin.cpp
	-$(CPPCHECK) Species.cpp
	-$(CPPCHECK) Creature.cpp
	-$(CPPCHECK) TestDarwin.cpp
	$(CXX) $(CXXFLAGS) Darwin.cpp Species.cpp Creature.cpp TestDarwin.cpp -o TestDarwin $(LDFLAGS)

# run/test files, compile with make all
FILES :=        \
    RunDarwin  \
    TestDarwin

# compile all
all: $(FILES)

# execute test harness with coverage
test: TestDarwin
	$(VALGRIND) ./TestDarwin
ifeq ($(shell uname -s), Darwin)
	$(GCOV) TestDarwin.cpp | grep -B 2 "cpp.gcov"
else
	gcc -c --coverage Darwin.cpp Creature.cpp Species.cpp
	$(GCOV) -b TestDarwin-Darwin.cpp TestDarwin-Creature.cpp TestDarwin-Species.cpp | grep -B 4 "cpp.gcov"
endif

# clone the Darwin test repo
../cs371p-Darwin-tests:
	git clone https://gitlab.com/DaSniper12/cs371p-Darwin-tests.git ../cs371p-Darwin-tests

# test files in the Darwin test repo
T_FILES := `ls ../cs371p-Darwin-tests/*.in.txt`

# generate a random input file
ctd-generate:
	for v in {1..100}; do $(CHECKTESTDATA) -g RunDarwin.ctd.txt >> RunDarwin.gen.txt; done

# execute the run harness against a test file in the Darwin test repo and diff with the expected output
../cs371p-Darwin-tests/%: RunDarwin
	$(CHECKTESTDATA) RunDarwin.ctd.txt $@.in.txt
	./RunDarwin < $@.in.txt > RunDarwin.tmp.txt
	diff RunDarwin.tmp.txt $@.out.txt

# execute the run harness against your test files in the Darwin test repo and diff with the expected output
run: ../cs371p-Darwin-tests
	-make ../cs371p-Darwin-tests/DaSniper12-RunDarwin

run-sample: RunDarwin
	./RunDarwin < sample.txt

# execute the run harness against all of the test files in the Darwin test repo and diff with the expected output
run-all: ../cs371p-Darwin-tests
	-for v in $(T_FILES); do make $${v/.in.txt/}; done

# auto format the code
format:
	$(ASTYLE) Darwin.cpp
	$(ASTYLE) Darwin.hpp
	$(ASTYLE) RunDarwin.cpp
	$(ASTYLE) TestDarwin.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
C_FILES :=          \
    .gitignore      \
    .gitlab-ci.yml  \
    Darwin.log.txt \
    html

# check the existence of check files
check: $(C_FILES)

# remove executables and temporary files
clean:
	rm -f  *.gcda
	rm -f  *.gcno
	rm -f  *.gcov
	rm -f  *.gen.txt
	rm -f  *.tmp.txt
	rm -f  RunDarwin
	rm -f  TestDarwin
	rm -rf *.dSYM

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Darwin.log.txt
	rm -f  Doxyfile
	rm -rf html
	rm -rf latex

# output versions of all tools
versions:
	uname -p

	@echo
	uname -s

	@echo
	which $(ASTYLE)
	@echo
	$(ASTYLE) --version

	@echo
	which $(CHECKTESTDATA)
	@echo
	$(CHECKTESTDATA) --version | head -n 1

	@echo
	which $(CPPCHECK)
	@echo
	$(CPPCHECK) --version

	@echo
	which $(DOXYGEN)
	@echo
	$(DOXYGEN) --version

	@echo
	which $(CXX)
	@echo
	$(CXX) --version | head -n 1

	@echo
	which $(GCOV)
	@echo
	$(GCOV) --version | head -n 1

	@echo
	which git
	@echo
	git --version

	@echo
	which make
	@echo
	make --version | head -n 1

ifneq ($(VALGRIND),)
	@echo
	which $(VALGRIND)
	@echo
	$(VALGRIND) --version
endif

	@echo
	which vim
	@echo
	vim --version | head -n 1

	@echo
	grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp

	@echo
	ls -al $(GTEST)/gtest.h
	@echo
	pkg-config --modversion gtest
	@echo
	ls -al $(LIB)/libgtest*.a

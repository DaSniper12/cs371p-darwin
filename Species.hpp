// -----------
// Species.hpp
// -----------

#ifndef Species_hpp
#define Species_hpp

// ----------------
// Species
// ----------------

#include <string>
#include <vector>
using namespace std;
#include "Instruction.hpp"
#include "Direction.hpp"

class Creature;

class Species
{
private:
  vector<pair<instructionType, unsigned>> instructions; // A direction and program counter
  char type;

public:
  Species();
  void addInstruction(char s);
  pair<instructionType, unsigned> getInstruction(unsigned);
  void turnLeft(direction &);
  void turnRight(direction &);
  void infect(Species **, Creature **);
  void hop(Creature **, Creature **);
  void ifEmpty(Creature **, unsigned &);
  void ifWall(Species **, unsigned &);
  void ifRandom(unsigned &);
  void ifEnemy(Species **, unsigned &);
  void go(unsigned &);
  bool execute(vector<Species **>, vector<Creature **>, Creature **, unsigned &, direction &);
  char typeOf();
};

#endif

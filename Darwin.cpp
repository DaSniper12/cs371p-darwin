// -----------
// Darwin.cpp
// -----------

// --------
// includes
// --------

#include "Darwin.hpp"
#include <iostream>
#include <map>
#include <cassert> // assert
using namespace std;

map<direction, std::pair<int, int>> directions = {
    {EAST, {0, 1}},
    {SOUTH, {1, 0}},
    {WEST, {0, -1}},
    {NORTH, {-1, 0}}
};

map<char, vector<pair<instructionType, unsigned>>> species_instructions = {
    {'f', {{LEFT, -1}, {GO, 0}}},
    {'h', {{HOP, -1}, {GO, 0}}},
    {'r', {{IF_ENEMY, 9}, {IF_EMPTY, 7}, {IF_RANDOM, 5}, {LEFT, -1}, {GO, 0}, {RIGHT, -1}, {GO, 0}, {HOP, -1}, {GO, 0}, {INFECT, -1}, {GO, 0}}},
    {'t', {{IF_ENEMY, 3}, {LEFT, -1}, {GO, 0}, {INFECT, -1}, {GO, 0}}}
};

Darwin::Darwin(unsigned row, unsigned col)
{
    assert(row > 0);
    assert(col > 0);

    grid.resize(row);
    for (unsigned i = 0; i < row; i++)
    {
        grid[i].resize(col);
    }
    wall_species.addInstruction('w');
    wallObj = Creature(&wall_species, 'e');
    wall = &wallObj;
};
void Darwin::addCreature(Creature *creature, Species *species, unsigned r, unsigned c)
{
    assert(r < grid.size());
    assert(c < grid[0].size());

    v_species.push_back(species);
    v_creatures.push_back(creature);
    grid[r][c] = v_creatures.back();
};
void Darwin::run()
{
    all_executed = !all_executed;

    for (size_t r = 0; r < grid.size(); r++)
    {
        for (size_t c = 0; c < grid[r].size(); c++)
        {
            if (grid[r][c] != nullptr) {
                bool control = true;
                while (control)
                {
                    vector<Creature **> nearby_creatures(4, nullptr);
                    for (size_t i = 0; i < 4; i++)
                    {
                        int new_r = r + directions[static_cast<direction>(i)].first;
                        int new_c = c + directions[static_cast<direction>(i)].second;
                        if (new_r < 0 || new_c < 0 || new_r >= grid.size() || new_c >= grid[0].size())
                        {
                            nearby_creatures[i] = &wall;
                        }
                        else
                        {
                            nearby_creatures[i] = &grid[new_r][new_c];
                        }
                    }

                    control = grid[r][c]->executeTurn(nearby_creatures, &grid[r][c], all_executed);
                }
            }
        }
    }
};

void Darwin::print() {
    cout << "  ";
    for (size_t i = 0; i < grid[0].size(); i++)
    {
        cout << i % 10;
    }
    cout << endl;

    for (size_t r = 0; r < grid.size(); r++)
    {
        cout << r % 10<< " ";
        for (size_t c = 0; c < grid[r].size(); c++)
        {
            if (grid[r][c] == nullptr)
            {
                cout << ".";
            }
            else
            {
                cout << grid[r][c]->typeOf();
            }
        }
        cout << endl;
    }
}
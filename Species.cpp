// -----------
// Species.cpp
// -----------

// --------
// includes
// --------

#include <iostream>
#include <map>
#include <cassert> // assert
using namespace std;
#include "Species.hpp"
#include "Creature.hpp"

Species::Species(){};
void Species::addInstruction(char s)
{
  type = s;
  instructions.clear();
  instructions = species_instructions[s];
};
void Species::turnLeft(direction &dir)
{
  switch (dir)
  {
  case EAST:
    dir = NORTH;
    break;
  case SOUTH:
    dir = EAST;
    break;
  case WEST:
    dir = SOUTH;
    break;
  case NORTH:
    dir = WEST;
    break;
  }
};
void Species::turnRight(direction &dir)
{
  switch (dir)
  {
  case EAST:
    dir = SOUTH;
    break;
  case SOUTH:
    dir = WEST;
    break;
  case WEST:
    dir = NORTH;
    break;
  case NORTH:
    dir = EAST;
    break;
  }
};
void Species::infect(Species **target_species, Creature **target)
{
  assert(target_species != nullptr);
  char type = this->typeOf();
  if ((*target_species)->typeOf() != type)
  {
    (*target)->reset_pc();
    (*target_species)->addInstruction(type);
  }
};
void Species::hop(Creature **target_creature, Creature **cur)
{
  assert(target_creature != nullptr);
  if (*target_creature == nullptr)
  {
    *target_creature = *cur;
    *cur = nullptr;
  }
};
void Species::ifEmpty(Creature **target_creature, unsigned &pc)
{
  assert(target_creature != nullptr);
  if (*target_creature == nullptr)
  {
    pc = instructions[pc].second;
    pc--;
  }
};
void Species::ifWall(Species **target_species, unsigned &pc)
{
  assert(target_species != nullptr);
  if ((*target_species)->type == 'w')
  {
    pc = instructions[pc].second;
    pc--;
  }
};
void Species::ifRandom(unsigned &pc)
{
  if (rand() % 2 == 1)
  {
    pc = instructions[pc].second;
    pc--;
  }
};
void Species::ifEnemy(Species **target_species, unsigned &pc)
{
  if (target_species != nullptr && (*target_species)->type != this->type && (*target_species)->type != 'w')
  {
    pc = instructions[pc].second;
    pc--;
  }
};
void Species::go(unsigned &pc)
{
  pc = instructions[pc].second;
  pc--;
};
bool Species::execute(vector<Species **> nearby_species, vector<Creature **> nearby_creatures, Creature **cur, unsigned &pc, direction &dir)
{
  assert(pc < instructions.size());
  Species **target_species = nullptr;
  Creature **target_creature = nullptr;
  switch (dir)
  {
  case EAST:
    target_species = &(*nearby_species[0]);
    target_creature = &(*nearby_creatures[0]);
    break;
  case SOUTH:
    target_species = &(*nearby_species[1]);
    target_creature = &(*nearby_creatures[1]);
    break;
  case WEST:
    target_species = &(*nearby_species[2]);
    target_creature = &(*nearby_creatures[2]);
    break;
  case NORTH:
    target_species = &(*nearby_species[3]);
    target_creature = &(*nearby_creatures[3]);
    break;
  }

  bool control = true;
  switch (instructions[pc].first)
  {
  case LEFT:
    control = false;
    turnLeft(dir);
    break;
  case RIGHT:
    control = false;
    turnRight(dir);
    break;
  case INFECT:
    control = false;
    infect(target_species, target_creature);
    break;
  case HOP:
    control = false;
    hop(target_creature, cur);
    break;
  case IF_EMPTY:
    ifEmpty(target_creature, pc);
    break;
  case IF_WALL:
    ifWall(target_species, pc);
    break;
  case IF_RANDOM:
    ifRandom(pc);
    break;
  case IF_ENEMY:
    ifEnemy(target_species, pc);
    break;
  case GO:
    go(pc);
    break;
  }
  pc++;
  return control;
};
char Species::typeOf()
{
  return type;
}
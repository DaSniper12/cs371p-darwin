// -----------
// Darwin.hpp
// -----------

#ifndef Darwin_hpp
#define Darwin_hpp

// ----------------
// Darwin
// ----------------

#include <vector>
using namespace std;
#include "Species.hpp"
#include "Creature.hpp"
#include "Instruction.hpp"
#include "Direction.hpp"

class Darwin
{
private:
    vector<Species *> v_species;
    vector<Creature *> v_creatures;
    vector<vector<Creature *>> grid;
    bool all_executed;
    Species wall_species;
    Creature wallObj;
    Creature *wall;

public:
    Darwin(unsigned, unsigned);
    void addCreature(Creature *, Species *, unsigned, unsigned);
    void run();
    void print();
};

#endif
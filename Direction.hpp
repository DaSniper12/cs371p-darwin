// -----------
// Direction.hpp
// -----------

#ifndef Direction_hpp
#define Direction_hpp

// ----------------
// Direction
// ----------------

#include <map>
using namespace std;

enum direction
{
  EAST,
  SOUTH,
  WEST,
  NORTH
};

extern map<direction, std::pair<int, int>> directions;

#endif

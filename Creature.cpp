// -----------
// Creature.cpp
// -----------

// --------
// includes
// --------

#include <iostream>
#include <map>
#include <cassert> // assert
#include <string>
using namespace std;
#include "Creature.hpp"
#include "Species.hpp"

Creature::Creature(){};
Creature::Creature(Species *p_species, char c)
{
  assert(p_species != nullptr);
  species = p_species;
  pc = 0;
  switch (c)
  {
  case 'e':
    dir = EAST;
    break;
  case 's':
    dir = SOUTH;
    break;
  case 'w':
    dir = WEST;
    break;
  case 'n':
    dir = NORTH;
    break;
  }
};
bool Creature::executeTurn(vector<Creature **> nearby_creatures, Creature **gridPtr, bool all_executed)
{
  assert(gridPtr != nullptr);
  bool control = false;
  if (all_executed != executed)
  {
    vector<Species **> nearby_species(4, nullptr);
    for (size_t i = 0; i < nearby_creatures.size(); i++)
    {
      if ((*nearby_creatures[i]) != nullptr)
      {
        nearby_species[i] = &(*nearby_creatures[i])->species;
      }
    }

    control = this->species->execute(nearby_species, nearby_creatures, gridPtr, pc, dir);
  }
  if (!control)
  {
    executed = all_executed;
  }
  return control;
};
char Creature::typeOf()
{
  return species->typeOf();
}
void Creature::reset_pc()
{
  pc = 0;
}

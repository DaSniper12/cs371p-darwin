// -----------
// Instruction.hpp
// -----------

#ifndef Instruction_hpp
#define Instruction_hpp

// ----------------
// Instruction
// ----------------

#include <map>
#include <vector>
using namespace std;

enum instructionType
{
  HOP,
  LEFT,
  RIGHT,
  INFECT,
  IF_EMPTY,
  IF_WALL,
  IF_RANDOM,
  IF_ENEMY,
  GO
};

extern map<char, vector<pair<instructionType, unsigned>>> species_instructions;

#endif

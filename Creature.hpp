// -----------
// Creature.hpp
// -----------

#ifndef Creature_hpp
#define Creature_hpp

// ----------------
// Creature
// ----------------

#include <vector>
using namespace std;
#include "Instruction.hpp"
#include "Direction.hpp"

class Species;

class Creature
{
private:
  Species *species;
  unsigned pc;
  direction dir;
  bool executed;

public:
  Creature();
  Creature(Species *, char);
  bool executeTurn(vector<Creature **>, Creature **, bool);
  char typeOf();
  void reset_pc();
};

#endif

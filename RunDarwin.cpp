// --------------
// RunDarwin.cpp
// --------------

// --------
// includes
// --------

#pragma once
#include <cassert>  // assert
#include <iostream> // cin, cout, endl
#include <vector>
#include <string>

#include "Darwin.hpp"

using namespace std;

// ----
// main
// ----

int main()
{
    unsigned n; // # of runs
    cin >> n;

    for (unsigned i = 0; i < n; i++) {
        srand(0);
        unsigned r, c;
        cin >> r >> c;
        Darwin d(r, c);

        unsigned num_creatures;
        cin >> num_creatures;

        Species species_arr[num_creatures];
        Creature creature_arr[num_creatures];
        int row_arr[num_creatures];
        int col_arr[num_creatures];

        for (unsigned j = 0; j < num_creatures; j++) {
            unsigned row, col;
            char species, dir;
            cin >> species >> row >> col >> dir;

            species_arr[j] = Species();
            species_arr[j].addInstruction(species);

            creature_arr[j] = Creature(&species_arr[j], dir);

            row_arr[j] = row;
            col_arr[j] = col;

        }

        for (unsigned j = 0; j < num_creatures; j++) {
            d.addCreature(&creature_arr[j], &species_arr[j], row_arr[j], col_arr[j]);
        }

        cout << "*** Darwin " << r << "x" << c << " ***" << endl;

        unsigned s,f;
        cin >> s >> f;

        int lastRun = s - (s % f);

        cout << "Turn = 0." << endl;
        d.print();
        if (s / f > 0 || i != n - 1) {
            cout << endl;
        }
        for (unsigned j = 0; j < s; j++) {
            d.run();
            if ((j+1) % f == 0) {
                cout << "Turn = " << j+1 << "." << endl;
                d.print();
                if (j + 1 != lastRun || i != n - 1)
                {
                    cout << endl;
                }
            }
        }
    }
    return 0;
}

// ---------------
// TestDarwin.cpp
// ---------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

// --------
// includes
// --------

#include "gtest/gtest.h"
#include <iostream>
#include <sstream>
#include <string>
#include "Species.hpp"
#include "Creature.hpp"
#include "Darwin.hpp"

using namespace std;

// ----------------
// Darwin class
// ----------------

TEST(DarwinFixture, test0) {
    unsigned row = 3;
    unsigned col = 3;
    Darwin d(row, col);

    streambuf* original_cout_buffer = cout.rdbuf();
    stringstream test_output;
    cout.rdbuf(test_output.rdbuf());

    d.print();

    cout.rdbuf(original_cout_buffer);
    string expected_output = "  012\n0 ...\n1 ...\n2 ...\n";
    ASSERT_EQ(test_output.str(), expected_output);
}
TEST(DarwinFixture, test1) {
    unsigned row = 3;
    unsigned col = 3;
    Darwin d(row, col);

    Species s = Species();
    s.addInstruction('f');
    Creature c = Creature(&s, 's');
    d.addCreature(&c, &s, 0, 0);

    streambuf* original_cout_buffer = cout.rdbuf();
    stringstream test_output;
    cout.rdbuf(test_output.rdbuf());

    d.print();

    cout.rdbuf(original_cout_buffer);
    string expected_output = "  012\n0 f..\n1 ...\n2 ...\n";
    ASSERT_EQ(test_output.str(), expected_output);
}
TEST(DarwinFixture, test2) {
    srand(0);
    unsigned row = 3;
    unsigned col = 3;
    Darwin d(row, col);

    Species s = Species();
    s.addInstruction('h');
    Creature c = Creature(&s, 's');
    d.addCreature(&c, &s, 0, 0);

    d.run();

    streambuf* original_cout_buffer = cout.rdbuf();
    stringstream test_output;
    cout.rdbuf(test_output.rdbuf());

    d.print();

    cout.rdbuf(original_cout_buffer);
    string expected_output = "  012\n0 ...\n1 h..\n2 ...\n";
    ASSERT_EQ(test_output.str(), expected_output);
}
TEST(DarwinFixture, test3) {
    srand(0);
    unsigned row = 3;
    unsigned col = 3;
    Darwin d(row, col);

    Species s = Species();
    s.addInstruction('h');
    Creature c = Creature(&s, 's');
    d.addCreature(&c, &s, 0, 0);

    d.run();
    d.run();

    streambuf* original_cout_buffer = cout.rdbuf();
    stringstream test_output;
    cout.rdbuf(test_output.rdbuf());

    d.print();

    cout.rdbuf(original_cout_buffer);
    string expected_output = "  012\n0 ...\n1 ...\n2 h..\n";
    ASSERT_EQ(test_output.str(), expected_output);
}
TEST(DarwinFixture, test4) {
    srand(0);
    unsigned row = 3;
    unsigned col = 3;
    Darwin d(row, col);

    Species s = Species();
    s.addInstruction('h');
    Creature c = Creature(&s, 'e');
    d.addCreature(&c, &s, 0, 0);

    d.run();

    streambuf* original_cout_buffer = cout.rdbuf();
    stringstream test_output;
    cout.rdbuf(test_output.rdbuf());

    d.print();

    cout.rdbuf(original_cout_buffer);
    string expected_output = "  012\n0 .h.\n1 ...\n2 ...\n";
    ASSERT_EQ(test_output.str(), expected_output);
}
TEST(DarwinFixture, test5) {
    srand(0);
    unsigned row = 3;
    unsigned col = 3;
    Darwin d(row, col);

    Species s = Species();
    s.addInstruction('h');
    Creature c = Creature(&s, 'e');
    d.addCreature(&c, &s, 0, 0);

    d.run();
    d.run();

    streambuf* original_cout_buffer = cout.rdbuf();
    stringstream test_output;
    cout.rdbuf(test_output.rdbuf());

    d.print();

    cout.rdbuf(original_cout_buffer);
    string expected_output = "  012\n0 ..h\n1 ...\n2 ...\n";
    ASSERT_EQ(test_output.str(), expected_output);
}
TEST(DarwinFixture, test6) {
    srand(0);
    unsigned row = 3;
    unsigned col = 3;
    Darwin d(row, col);

    Species s = Species();
    s.addInstruction('h');
    Creature c = Creature(&s, 's');
    d.addCreature(&c, &s, 0, 0);

    d.run();
    d.run();
    d.run();

    streambuf* original_cout_buffer = cout.rdbuf();
    stringstream test_output;
    cout.rdbuf(test_output.rdbuf());

    d.print();

    cout.rdbuf(original_cout_buffer);
    string expected_output = "  012\n0 ...\n1 ...\n2 h..\n";
    ASSERT_EQ(test_output.str(), expected_output);
}
TEST(DarwinFixture, test7) {
    srand(0);
    unsigned row = 3;
    unsigned col = 3;
    Darwin d(row, col);

    Species s = Species();
    s.addInstruction('r');
    Creature c = Creature(&s, 's');
    d.addCreature(&c, &s, 0, 0);

    streambuf* original_cout_buffer = cout.rdbuf();
    stringstream test_output;
    cout.rdbuf(test_output.rdbuf());

    d.run();
    d.run();

    d.print();
    string expected_output = "  012\n0 ...\n1 ...\n2 r..\n";
    ASSERT_EQ(test_output.str(), expected_output);
    test_output.str("");

    d.run();
    d.run();
    d.run();
    d.run();
    d.run();

    d.print();
    expected_output = "  012\n0 ...\n1 r..\n2 ...\n";
    ASSERT_EQ(test_output.str(), expected_output);

    cout.rdbuf(original_cout_buffer);
}
TEST(DarwinFixture, test8) {
    srand(0);
    unsigned row = 3;
    unsigned col = 3;
    Darwin d(row, col);

    Species s = Species();
    s.addInstruction('r');
    Creature c = Creature(&s, 's');
    d.addCreature(&c, &s, 1, 1);

    streambuf* original_cout_buffer = cout.rdbuf();
    stringstream test_output;
    cout.rdbuf(test_output.rdbuf());

    d.run();

    d.print();
    string expected_output = "  012\n0 ...\n1 ...\n2 .r.\n";
    ASSERT_EQ(test_output.str(), expected_output);
    test_output.str("");

    d.run();
    d.run();

    d.print();
    expected_output = "  012\n0 ...\n1 ...\n2 r..\n";
    ASSERT_EQ(test_output.str(), expected_output);
    test_output.str("");

    d.run();
    d.run();
    d.run();
    d.run();

    d.print();
    expected_output = "  012\n0 ...\n1 r..\n2 ...\n";
    ASSERT_EQ(test_output.str(), expected_output);

    cout.rdbuf(original_cout_buffer);
}
TEST(DarwinFixture, test9) {
    srand(0);
    unsigned row = 3;
    unsigned col = 3;
    Darwin d(row, col);

    Species s = Species();
    s.addInstruction('r');
    Creature c = Creature(&s, 'e');
    d.addCreature(&c, &s, 0, 0);

    streambuf* original_cout_buffer = cout.rdbuf();
    stringstream test_output;
    cout.rdbuf(test_output.rdbuf());

    d.run();
    d.run();

    d.print();
    string expected_output = "  012\n0 ..r\n1 ...\n2 ...\n";
    ASSERT_EQ(test_output.str(), expected_output);
    test_output.str("");

    d.run();
    d.run();

    // d.print();
    // expected_output = "  012\n0 ...\n1 ..r\n2 ...\n";
    // ASSERT_EQ(test_output.str(), expected_output);
    // test_output.str("");

    d.run();

    d.print();
    expected_output = "  012\n0 ...\n1 ...\n2 ..r\n";
    ASSERT_EQ(test_output.str(), expected_output);

    cout.rdbuf(original_cout_buffer);
}


// ----------------
// Creature class
// ----------------

TEST(CreatureFixture, test0) {
    Species s = Species();
    s.addInstruction('f');
    Creature c = Creature(&s, 'e');
    ASSERT_EQ(c.typeOf(), 'f');
}
TEST(CreatureFixture, test1) {
    Species s = Species();
    s.addInstruction('h');
    Creature c = Creature(&s, 'e');
    ASSERT_EQ(c.typeOf(), 'h');
}
TEST(CreatureFixture, test2) {
    Species s = Species();
    s.addInstruction('r');
    Creature c = Creature(&s, 'e');
    ASSERT_EQ(c.typeOf(), 'r');
}
TEST(CreatureFixture, test3) {
    Species s = Species();
    s.addInstruction('t');
    Creature c = Creature(&s, 'e');
    ASSERT_EQ(c.typeOf(), 't');
}
TEST(CreatureFixture, test4) {
    Species s = Species();
    s.addInstruction('w');
    Creature c = Creature(&s, 'e');
    ASSERT_EQ(c.typeOf(), 'w');
}

// ----------------
// Species class
// ----------------

TEST(SpeciesFixture, test0) {
    Species s = Species();
    s.addInstruction('f');
    ASSERT_EQ(s.typeOf(), 'f');
}
TEST(SpeciesFixture, test1) {
    Species s = Species();
    s.addInstruction('h');
    ASSERT_EQ(s.typeOf(), 'h');
}
TEST(SpeciesFixture, test2) {
    Species s = Species();
    s.addInstruction('r');
    ASSERT_EQ(s.typeOf(), 'r');
}
TEST(SpeciesFixture, test3) {
    Species s = Species();
    s.addInstruction('t');
    ASSERT_EQ(s.typeOf(), 't');
}
TEST(SpeciesFixture, test4) {
    Species s = Species();
    s.addInstruction('w');
    ASSERT_EQ(s.typeOf(), 'w');
}